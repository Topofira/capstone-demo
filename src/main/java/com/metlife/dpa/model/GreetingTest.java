package com.metlife.dpa.model;

import static org.junit.Assert.*;

import java.math.BigDecimal;

import org.junit.Test;

public class GreetingTest {
	
	@Test
	public final void test() {
		String _content  = "testme";
		Greeting greet = new Greeting();
		greet.setContent(_content);
		assertEquals(greet.getContent(),_content);
	}

}
