package com.metlife.dpa.model;

import static org.junit.Assert.*;

import java.math.BigDecimal;

import org.junit.Test;

public class BreedTest {
	
	@Test
	public final void test() {
		String _content  = "Pug";
		Breed doggie = new Breed(_content);
		assertEquals(doggie.getContent(),_content);
		assertEquals(doggie.getImage(),"http://www.dogbreedslist.info/uploads/allimg/dog-pictures/Pug-2.jpg");
	}

}
