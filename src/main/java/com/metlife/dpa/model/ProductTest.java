package com.metlife.dpa.model;

import static org.junit.Assert.*;

import java.math.BigDecimal;

import org.junit.Test;

public class ProductTest {
	
	@Test
	public final void test() {
		BigDecimal _price = new BigDecimal(10000);
		Product car = new Product("VW",_price);
		assertEquals(car.price,_price);
		assertEquals(car.name,"VW");
	}

}
