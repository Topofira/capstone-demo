package com.metlife.dpa.model;


public class Greeting {

    private String content;
    
    public Greeting(){
    	super();
    }
    

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

}
