package com.metlife.dpa.model;

import java.util.HashMap;

public class Breed extends Greeting {

	private static HashMap<String,String> images = new HashMap<String,String>();
	

    public Breed(){
    	super();
    }
    
	
    public Breed(String myContent){
    	this.setContent(myContent);
    	Breed.setImages();
    }
    
    private static void setImages(){
    	images.put("Corgi", "http://img2.timeinc.net/people/i/2013/pets/news/131118/corgi-3-600.jpg");
    	images.put("Beagle", "http://assets.dogtime.com/breed/profile/image/4d3771f10106195669001f8c/max_400_beagle.jpg");
    	images.put("Pug","http://www.dogbreedslist.info/uploads/allimg/dog-pictures/Pug-2.jpg");
    	images.put("Poodle","http://www.wagoutfitters.com/images/poodle.jpg");
    	images.put("Shiba Inu", "http://shibainudog.net/wp-content/uploads/shiba-inu-puppies3.jpg");
    	images.put("Basset Hound", "http://d3o47n6kn1r59u.cloudfront.net/images/dogbreeds/large/Basset-Hound.jpg");
    	images.put("Labrador Retriever", "http://alex-cool.com/wp-content/uploads/Labrador_Retriever_yellow_puppy-768x1024.jpg");
    }
    
    public String getImage(){
    	setContent(findMyBreed());
    	return images.get(this.getContent());
    }
    
    private String findMyBreed(){
    	String s = this.getContent();
    	if (Character.isLowerCase(s.charAt(0)))
    	{
    		s = Character.toUpperCase(s.charAt(0)) + s.substring(1);
    	}
    	if (s.equals("Shiba") || s.equals("Shibainu"))
    	{
    		s = "Shiba Inu";
    	}
    	else if (s.equals("Hound") || s.equals("Basset"))
    	{
    		s = "Basset Hound";
    	}
    	else if (s.equals("Lab") || s.equals("Labrador") || s.equals("Retriever"))
    	{
    		s = "Labrador Retriever";
    	}
    	return s;
    }
}
