package com.metlife.dpa.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.metlife.dpa.model.Breed;
import com.metlife.dpa.model.Cat;

@Controller
public class CatController {

	Cat myCat= new Cat();
	
	
	@RequestMapping(value="/cat", method=RequestMethod.GET)
	public String CatForm(Model model){
		model.addAttribute("catForm", myCat);
		return "cat";
	}
	
    @RequestMapping(value="/cat", method=RequestMethod.POST)
    public String greetingSubmit(@ModelAttribute Cat cat, Model model) {
        model.addAttribute("cat", cat);
        return "catResult";
    }
}
