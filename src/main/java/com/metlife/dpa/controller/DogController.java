package com.metlife.dpa.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.metlife.dpa.model.Breed;

@Controller
public class DogController {

	Breed myBreed = new Breed("Corgi");
	
	
	@RequestMapping(value="/dog")
	public String sayHello(Model model){
		model.addAttribute("breed", "Hello " + myBreed.getContent());
		model.addAttribute("pic", myBreed.getImage());
		return "dog";
	}
	
    @RequestMapping(value="/breedSelector", method=RequestMethod.GET)
    public String greetingForm(Model model) {
        model.addAttribute("formBreed", myBreed);
        return "breedSelector";
    }

    @RequestMapping(value="/breedSelector", method=RequestMethod.POST)
    public String greetingSubmit(@ModelAttribute Breed breed, Model model) {
        model.addAttribute("breedName", breed.getContent());
        model.addAttribute("breedImg", breed.getImage());
        myBreed.setContent(breed.getContent());
        return "dogResult";
    }
}
