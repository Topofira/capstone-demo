<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>You got Cat!</title>
</head>
<body>
<p>You know  ${ cat.name }?</p>
<p>Yeah, it's ${ cat.age } years old!</p>
</body>
</html>